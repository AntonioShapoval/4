﻿#include <iostream>
#include <windows.h>
#include <string>
#include <filesystem>

// Функція для зміни часу створення файлу
bool SetFileCreationTime(const std::wstring& filePath, const SYSTEMTIME& newTime) {
    HANDLE hFile = CreateFileW(filePath.c_str(), GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE) {
        std::wcerr << L"Не вдалося відкрити файл: " << filePath << std::endl;
        return false;
    }

    FILETIME ft;
    if (!SystemTimeToFileTime(&newTime, &ft)) {
        std::wcerr << L"Не вдалося конвертувати системний час у файловий час." << std::endl;
        CloseHandle(hFile);
        return false;
    }

    if (!SetFileTime(hFile, &ft, NULL, NULL)) {
        std::wcerr << L"Не вдалося змінити час створення файлу: " << filePath << std::endl;
        CloseHandle(hFile);
        return false;
    }

    CloseHandle(hFile);
    return true;
}

int main() {
    std::wstring directoryPath = L"X:\\Нова папка (9)\\365\\4\\34"; // Замініть цей шлях на потрібний
    SYSTEMTIME newTime;
    // Встановіть новий час створення
    newTime.wYear = 6666;
    newTime.wMonth = 6;
    newTime.wDay = 6;
    newTime.wHour = 3;
    newTime.wMinute = 6;
    newTime.wSecond = 6;
    newTime.wMilliseconds = 0;

    for (const auto& entry : std::filesystem::directory_iterator(directoryPath)) {
        if (entry.is_regular_file()) {
            std::wstring filePath = entry.path().wstring();
            if (SetFileCreationTime(filePath, newTime)) {
                std::wcout << L"Час створення файлу змінено: " << filePath << std::endl;
            }
            else {
                std::wcerr << L"Не вдалося змінити час створення файлу: " << filePath << std::endl;
            }
        }
    }

    return 0;
}
